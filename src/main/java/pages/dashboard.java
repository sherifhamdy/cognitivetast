package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import baseActions.actions;
import mainConfiguration.configuration;

public class dashboard 
{
	// Object Repository

	private static String[] LanguageListArea ={"xpath",".//*[@id='preloader']/../*[5]/*[1]/*[2]/*[2]/*[1]/*[4]/*[1]"};

	private static String[] myAccountBTN={"xpath",".//*[@id='preloader']/../*[5]//*[@id='li_myaccount']/a"};  		   // another option ->  (//a[contains(text(),'My Account')])[2]
	private static String[] signUpBTN={"xpath",".//*[@id='preloader']/../*[5]//*[@id='li_myaccount']/ul/*[2]"};  	  // another option -> (//a[contains(text(),'Sign Up')])[2]
	//private static String[] signInBTN={"xpath",".//*[@id='preloader']/../*[5]//*[@id='li_myaccount']/ul/*[1]"};		 // another option -> (//a[contains(text(),'Login')])[2]

	private static String[] firstNAME = {"name","firstname"};
	private static String[] lastNAME = {"name","lastname"};
	private static String[] phoneNUM = {"name","phone"};
	private static String[] emailACCOUNT = {"name","email"};
	private static String[] passWORD = {"name","password"};
	private static String[] confpassWORD = {"name","confirmpassword"};
	private static String[] signUpSubmit={"xpath",".//*[@id='headersignupform']/*[9]/*[1]"};

	private static String[] HiName ={"xpath",".//*[@id='body-section']/*[1]/*[1]/*[1]/*[1]/*[2]"};
	//private static String[] counterTime = {"id","txt"};

	private static String[] hotel={"xpath",".//*[@class='text-center']/../../*[2]"};
	private static String[] hotelField ={"xpath",".//*[@id='dpd1']/../*[1]/*[1]/*[3]/*[2]"};
	private static String[] checkIN ={"name","checkin"};
	private static String[] checkOUT={"name","checkout"};
	private static String[] searchBTN ={"xpath",".//*[@name='module_type']/../*[3]"};

	private static String[] HotelCityResultList={"xpath",".//*[@id='select2-drop']//*[@class='select2-result-sub']"};


	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	//Method to select the needed language from the top header list
	public static void language(String lang)
	{
		String languageCode = actions.getLanguageCode(lang);		
		String[] languageLocator = {"xpath",".//*[@id='preloader']/../*[5]//*[@id='"+languageCode+"']"};
		actions.waitForElement(LanguageListArea);
		Actions actionsz = new Actions(configuration.driver);
		actionsz.moveToElement(actions.locateElement(LanguageListArea)).perform();
		actions.waitForElement(languageLocator);
		actionsz.click(actions.locateElement(languageLocator)).perform();
	}


	//Method to tab on the Hotel section at the top header
	public static void clickOnHotelSection()
	{
		actions.waitForElement(hotel);
		actions.clickOnElement(hotel);
	}


	//Method to click and type the hotel or city name and wait the result
	public static void TypeHotelOrCity(String HoteloOrCity) throws InterruptedException
	{
		actions.waitForElement(hotelField);
		actions.ClickTypeElement(hotelField,HoteloOrCity,HotelCityResultList);
	}


	//Method to type the checkIN date
	public static void CheckIN(String checkInDate)
	{
		actions.waitForElement(checkIN);
		actions.typeOnElement(checkIN, checkInDate);	
	}


	//Method to type the checkout date 
	public static void CheckOut(String checkOutDate)
	{
		actions.waitForElement(checkIN);
		actions.typeOnElement(checkOUT, checkOutDate);	
	}


	// Method to click on search button to display the details
	public static void clickOnSearch()
	{
		actions.clickOnElement(searchBTN);
	}


	// Method to validate if the selected language format is displayed properly after logging in
	public static boolean loggedIN_withLanguage(String lang)
	{
		actions.waitForElement(HiName);
		WebElement Hi = actions.locateElement(HiName);
		boolean flag = false;
		switch(lang)
		{
		case "Farsi":
			if(Hi.getText().trim().contains("سلام")== true)
			{
				flag = true;
			}
			else
			{
				flag = false;
			}
			break;
		case "Arabic":
			if(Hi.getText().trim().contains("مرحبا")== true)
			{
				flag = true;
			}
			else
			{
				flag = false;
			}
			break;
		case "Turkish":
			if(Hi.getText().trim().contains("Merhaba")== true)
			{
				flag = true;
			}
			else
			{
				flag = false;
			}
			break;
		case "French":
			if(Hi.getText().trim().contains("Salut")== true)
			{
				flag = true;
			}
			else
			{
				flag = false;
			}
			break;
		case "Spanish":
			if(Hi.getText().trim().contains("Hola")== true)
			{
				flag = true;
			}
			else
			{
				flag = false;
			}
			break;
		case "Russian":
			if(Hi.getText().trim().contains("Привет")== true)
			{
				flag = true;
			}
			else
			{
				flag = false;
			}
			break;

		case "English":
			if(Hi.getText().contains("Hi,")== true)
			{
				flag = true;
			}
			else
			{
				flag = false;
			}
			break;		

		}
		return flag;
	}


	// Not needed anymore for validate logging in cuz of loggedIN_withLanguage method
	public static boolean oldloggedIN()
	{
		actions.waitForElement(HiName);
		//WebElement counter = actions.locateElement(counterTime);
		WebElement Hi = actions.locateElement(HiName);
		boolean flag = false;

		if(Hi.getText().contains("Hi,")== true)
		{
			flag = true;
		}
		else
		{
			flag = false;
		}
		/*
		if (counter.isDisplayed() == true )
		{
			String dateTime = actions.getCurrentDataTime();
			String returned_dateTime = counter.getText();
			if(returned_dateTime.contains(dateTime))
			{
			flag = true;
			}
			else
			{
				flag = false;
			}
		}
		else
		{
			flag =false;
		}
		 */
		return flag;

	}


	//Method to click on my account
	public static void clickOnMyAccount() throws InterruptedException
	{
		actions.waitForElement(myAccountBTN);
		actions.clickOnElement(myAccountBTN);
	}


	//Method to select sign up option
	public static void clickOnSignUp()
	{
		actions.waitForElement(signUpBTN);
		actions.clickOnElement(signUpBTN);
	}


	//Method to fill the data 
	public static void fillData(String firstName, String lastName , String phone , String Email,  String pass, String confPASS)
	{
		actions.waitForElement(firstNAME);
		actions.typeOnElement(firstNAME, firstName);
		actions.typeOnElement(lastNAME, lastName);
		actions.typeOnElement(phoneNUM, phone);
		actions.typeOnElement(emailACCOUNT, Email);
		actions.typeOnElement(passWORD, pass);
		actions.typeOnElement(confpassWORD, confPASS);
	}


	//Method to submit all data for signing up
	public static void signUpSubmit()
	{
		actions.waitForElement(signUpSubmit);
		actions.clickOnElement(signUpSubmit);
	}

}
