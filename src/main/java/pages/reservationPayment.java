package pages;


import baseActions.actions;
import mainConfiguration.configuration;

public class reservationPayment 
{
	// Object Repository

	private static String[] payNowBTN={"xpath",".//*[@id='body-section']/div[1]/div[2]/div[2]/center/button[2]"}; // or //  "cssSelector","button.btn.btn-primary"    
	private static String[] paymentList={"xpath",".//*[@id='gateway']"};
	private static String[] cardNumField={"id","card-number"};
	private static String[] cardFirstName={"id","card-holder-firstname"};
	private static String[] cardLastName={"id","card-holder-lastname"};
	private static String[] finalPayBTN={"xpath","//*[@id='bookingid']/../button"};  	
	private static String[] invalidERROR={"xpath","//*[@id='creditcardgateway']/../*[1]"};
	private static String[] OnArrivalBTN={"xpath",".//*[@id='pay']/../*[2]/*[1]/*[1]"};
	private static String[] reserved = {"xpath",".//*[@id='invoiceTable']/*[1]/*[1]/*[1]/*[1]/*[1]"};


	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	// Method to validate booking completion and the payment method is ready 
	public static boolean reservationComplete()
	{
		actions.waitForElement(payNowBTN);

		boolean flag = false;
		if(actions.locateElement(payNowBTN).isDisplayed() == true)
		{
			flag = true;
		}
		else
		{
			flag = false;
		}
		return flag;
	}

	//Method to validate credit card option fire an error 
	public static boolean payByCreditCard(String cardFN,String cardLN, String invalidCardNum)
	{
		actions.waitForElement(payNowBTN);
		actions.clickOnElement(payNowBTN);
		actions.waitForElement(paymentList);
		actions.selectOptions(paymentList, 4);
		actions.waitForElement(cardNumField);
		actions.typeOnElement(cardFirstName, cardFN);
		actions.typeOnElement(cardLastName, cardLN);
		actions.typeOnElement(cardNumField, invalidCardNum);
		actions.clickOnElement(finalPayBTN);

		boolean flag=false;
		if(actions.locateElement(invalidERROR).isDisplayed()==true)
		{
			flag=true;
		}
		else
		{
			flag = false;
		}
		return flag;
	}

	//Method to validate for pay on arrival button functionality, redirects to the dashboard
	public static boolean payOnArrival()
	{
		actions.clickOnElement(OnArrivalBTN);
		configuration.driver.switchTo().alert().accept();

		actions.waitForElement(reserved);
		boolean reservedWordDisplayed = actions.locateElement(reserved).isDisplayed();
		boolean reservedWordTxt = actions.locateElement(reserved).getText().trim().equalsIgnoreCase("reserved");
		boolean flag = false;
		if(reservedWordDisplayed == true && reservedWordTxt == true)
		{		
			flag=true;		
		}
		else
		{
			flag = false;
		}
		return flag;
	}


}
