package pages;

import org.openqa.selenium.WebElement;

import baseActions.actions;

public class hotels 
{
	// Object Repository
	private static String[] filterSearchLabel={"xpath",".//*[@class='filterstext']"};  	
	private static String[] hotelDetails={"xpath",".//*[@class='RTL']"};
	private static String[] roomCheckBox={"xpath",".//*[@class='control__indicator']"};
	private static String[] roomAvailabilityBTN={"xpath",".//*[@class='control__indicator']/../../../../*[1]/*[2]"};
	private static String[] NoRooms ={"xpath",".//*[@class='control__indicator']/../../../../../..//select"};
	private static String[] selectDates={"xpath",".//*[@id='53']"};
	private static String[] BookNowBTN ={"xpath","//*[@id='ROOMS']/div/button"};
	private static String[] extrasLabel={"xpath","//*[@id='bookingdetails']/div[2]/div"};
	private static String[] flowerToggle={"xpath","//*[@id='3']/../span"};
	private static String[] confirmBookBTN={"xpath","//*[@id='waiting']/../button"};

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	//Method to validate if the details screen after search button clicked is displayed 
	public static boolean HotelDetailsPage()
	{
		actions.waitForElement(filterSearchLabel);
		WebElement filterSearchLBL = actions.locateElement(filterSearchLabel);
		boolean flag = false;
		if(filterSearchLBL.isDisplayed()== true)
		{
			flag = true;
		}
		else
		{
			flag = false;
		}
		return flag;

	} 
	//Method to click on the details of any hotel row 
	public static void clickOnHotelResultRow(int HotelRowNum)
	{

		String[] details1stResult ={"xpath",".//*[@class='img_list']/../../../../*["+HotelRowNum+"]/*[1]/*[3]/a/button"}; // after last parent the number of hotel row 1st,2nd,3rd...etc

		actions.waitForElement(details1stResult);
		actions.clickOnElement(details1stResult);
	}

	//Method to validate if the details for the selected hotel is displayed
	public static boolean hotelDetailsLoaded()
	{
		actions.waitForElement(hotelDetails);
		boolean hotelDetailsDisplayed = actions.locateElement(hotelDetails).isDisplayed();
		boolean flag = false;
		if(hotelDetailsDisplayed == true)
		{
			flag = true;
		}
		else
		{
			flag = false;
		}
		return flag;
	}

	//Calling this method in (EditThenBookNowSelectedHotel) to continue edit in the room specifications (HOTEL-ROOM)
	private static void editInHotel(int NoRooms , int DateOption)
	{
		actions.scrollDown(roomCheckBox);
		actions.clickOnElement(roomCheckBox);

		actions.selectOptions(hotels.NoRooms, NoRooms);

		actions.clickOnElement(roomAvailabilityBTN);
		actions.waitForElement(roomAvailabilityBTN);

		actions.selectOptions(selectDates,DateOption);

	}
	//Method to edit in the room specifications (HOTEL-ROOM)
	public static void EditThenBookNowSelectedHotel(int NoRooms , int DateOption)
	{
		editInHotel(NoRooms,DateOption);

		actions.scrollDown(BookNowBTN);
		actions.clickOnElement(BookNowBTN);

	}

	//Method to validate the page of extras and booking confirmation is displayed (HOTEL-ROOM)
	public static boolean extraBookingDetails()
	{
		actions.waitForElement(extrasLabel);
		boolean flag = false;
		if(actions.locateElement(extrasLabel).isDisplayed() == true)
		{
			flag = true;
		}
		else
		{
			flag = false;
		}
		return flag;
	}

	//Add some extras then confirm the final booking (HOTEL-ROOM)
	public static void confirmBookingHotelRoom()
	{
		actions.scrollDown(flowerToggle);
		actions.locateElement(flowerToggle).click();
		actions.scrollDown(confirmBookBTN);
		actions.clickOnElement(confirmBookBTN);
	}

}
