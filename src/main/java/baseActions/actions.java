package baseActions;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import mainConfiguration.configuration;

public class actions 
{


	// method for scrolling down till reach the desired element
	public static void scrollDown(String[]element)
	{

		try
		{
			Actions actionScroll = new Actions(configuration.driver);
			actionScroll.moveToElement(actions.locateElement(element)).perform();
			actions.waitForElement(element);
		}
		catch(Exception e)
		{
			System.out.println(e);
		}

	}

	public static void selectOptions(String[] element,int ascending)
	{
		WebElement elementLocated = actions.locateElement(element);
		Select dropDown = new Select(elementLocated);
		dropDown.selectByIndex(ascending);

	}

	//Method to take screenshot when Test cases Fail
	public static void captureFAIL_Screenshot(org.openqa.selenium.WebDriver driver , String screenshotname)
	{
		//Save the capture screen shot into the destination created folder with the name of the test case 
		Path dest = Paths.get("./Fail_Screenshots",screenshotname+".png");
		try {
			Files.createDirectories(dest.getParent());
			FileOutputStream out = new FileOutputStream(dest.toString());
			out.write(((org.openqa.selenium.TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES));
			out.close();
		} catch (IOException e) {
			System.out.print("Exception while taking screen shot" + e.getMessage());
		}
	}
	//Method to take screenshot when Test cases Fail
	public static void capturePASS_Screenshot(org.openqa.selenium.WebDriver driver , String screenshotname)
	{
		//Save the capture screen shot into the destination created folder with the name of the test case 
		Path dest = Paths.get("./Pass_Screenshots",screenshotname+".png");
		try {
			Files.createDirectories(dest.getParent());
			FileOutputStream out = new FileOutputStream(dest.toString());
			out.write(((org.openqa.selenium.TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES));
			out.close();
		} catch (IOException e) {
			System.out.print("Exception while taking screen shot" + e.getMessage());
		}
	}


	//Method to locate a specific element
	public static WebElement locateElement(String[]element)
	{
		WebElement elementLocated = configuration.driver.findElement(getObjectLocator(element[0], element[1]));
		return elementLocated;
	}


	// Method to click on an element
	public static void clickOnElement(String[]element)
	{
		WebElement Element = configuration.driver.findElement(getObjectLocator(element[0], element[1]));
		Element.click();
	}


	// Method to click on an element
	public static void ClickTypeElement(String[]element,String HoteloOrCity,String[]element2Wait) throws InterruptedException
	{
		Actions ac = new Actions(configuration.driver);
		ac.click(actions.locateElement(element)).sendKeys(HoteloOrCity).perform();  // before -> configuration.driver.findElement(getObjectLocator(element[0], element[1]))
		waitForElement(element2Wait);
		ac.sendKeys(Keys.ENTER).build().perform();
	}


	// Method to clear then Type on an element 
	public static WebElement typeOnElement(String[]element,String value)
	{
		WebElement Element = configuration.driver.findElement(getObjectLocator(element[0], element[1]));
		Element.clear();
		Element.sendKeys(value);
		return Element;
	}


	// Method to wait for an element by visibility 
	public  static void waitForElement(String[]element)
	{		    
		WebDriverWait wait = new WebDriverWait(configuration.driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(getObjectLocator(element[0], element[1])));
	}


	//Method to go to the desired URL
	public static void WriteANDGotoTheURL()
	{
		configuration.driver.get(configuration.APP_URL);
	}


	//Method to get the code of the desired language
	public static String getLanguageCode(String language)
	{
		String languageCode=null;
		switch(language)
		{
		case "Farsi":
			languageCode="fa";
			break;
		case "Arabic":
			languageCode="ar";
			break;
		case "Turkish":
			languageCode="tr";
			break;
		case "French":
			languageCode="fr";
			break;
		case "Spanish":
			languageCode="es";
			break;
		case "Russian":
			languageCode="ru";
			break;
		case "English":
			languageCode="en";
			break;

		}
		return languageCode;
	}


	//Method for Locators (By,Value)
	public  static By getObjectLocator(String locatorType,String locatorValue)
	{
		By locator = null;

		switch(locatorType)
		{
		case "id":
			locator = By.id(locatorValue);
			break;
		case "name":
			locator = By.name(locatorValue);
			break;
		case "cssSelector":
			locator = By.cssSelector(locatorValue);
			break;
		case "linkText":
			locator = By.linkText(locatorValue);
			break;
		case "partialLinkText":
			locator = By.partialLinkText(locatorValue);
			break;
		case "tagName":
			locator = By.tagName(locatorValue);
			break;
		case "xpath":
			locator = By.xpath(locatorValue);
			break;
		}
		return locator;
	}
}
