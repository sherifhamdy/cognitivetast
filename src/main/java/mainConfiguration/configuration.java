package mainConfiguration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import baseActions.actions;
public class configuration 
{

	public static WebDriver driver;
	public static String APP_URL = "https://www.phptravels.net/";

	// initialize google Chrome_driver
	@BeforeSuite
	public static WebDriver setup()
	{
		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\resources\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.navigate().to(APP_URL);
		return driver;
	}

	// close the browser
	@AfterSuite
	public static void quitBrowser()
	{
		driver.quit();
	}

	//Take Screen-shot when test case fails and add it to the screen-shot folder
	@AfterMethod
	public static void screenshotOnFailure(ITestResult result)
	{
		if(result.getStatus() == ITestResult.FAILURE)
		{
			System.out.println("Failed");
			System.out.println("taking Fail Screenshot...");
			actions.captureFAIL_Screenshot(driver, result.getName());
		}

	}
	@AfterMethod
	public static void screenshotOnSuccess(ITestResult result)
	{
		if(result.getStatus() == ITestResult.SUCCESS)
		{
			System.out.println("Passed");
			System.out.println("taking Pass Screenshot...");
			actions.capturePASS_Screenshot(driver, result.getName());
		}
	}
}
