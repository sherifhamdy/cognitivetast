package CognitevAutomationTask.CognitevAutomationTask;

import static org.testng.Assert.assertEquals;

import java.io.IOException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import data.ExcelReader;
import data.LoadProperties;
import mainConfiguration.configuration;
import pages.hotels;
import pages.reservationPayment;
import pages.dashboard;

public class TestCases extends configuration
{
	/*
	@DataProvider(name="ExcelDataReader")
	public Object[][] SearchData() throws IOException
	{
		// get data from Excel Reader class 
		ExcelReader ER = new ExcelReader();
		return ER.getExcelData();
	}


	@DataProvider(name="ExcelDataReader")
	public static Object[][] userData()
	{
		return new Object[][] {
			{"English","TestFN" , "TestLN","01111111111","testxxx996@gmail.com","123456","123456"}

		};
	}
	 */


	// Read data from user data file 
	String FirstName = LoadProperties.userData.getProperty("firstname");
	String LastName = LoadProperties.userData.getProperty("lastname");
	String Phone = LoadProperties.userData.getProperty("phone");
	String Email = LoadProperties.userData.getProperty("email"); 
	String Password = LoadProperties.userData.getProperty("password"); 
	String ConfirmPass = LoadProperties.userData.getProperty("Confpassword"); 


	@Test(priority=1,enabled=true)
	public void Registration() throws InterruptedException
	{
		//Available languages: Farsi,Arabic,Turkish,French,Spanish,Russian,English
		dashboard.language("English"); 																	 //Capital First Letter
		dashboard.clickOnMyAccount();		
		dashboard.clickOnSignUp();
		dashboard.fillData(FirstName, LastName,Phone,Email,Password,ConfirmPass);
		dashboard.signUpSubmit();

		boolean actual = dashboard.loggedIN_withLanguage("English");   									//Capital First Letter
		boolean expected=true;
		assertEquals(actual, expected);
		System.out.println("Signed in properly after registration");

	}


	@Test(priority=2,enabled=true, dependsOnMethods={"Registration"})
	public void HotelSearch() throws InterruptedException
	{		
		dashboard.clickOnHotelSection();
		dashboard.TypeHotelOrCity("Cairo");  														//Capital First Letter
		dashboard.CheckIN("20/12/2018");   														   //Format DD/MM/YYYY
		dashboard.CheckOut("30/12/2018");  														  //Format DD/MM/YYYY
		dashboard.clickOnSearch();

		boolean actual = hotels.HotelDetailsPage();
		boolean expected=true;
		assertEquals(actual, expected);
		System.out.println("Hotel Details Screen is displayed successfully");
	}



	@Test(priority=3,enabled=true, dependsOnMethods={"HotelSearch"})
	public void HotelSelection()
	{
		hotels.clickOnHotelResultRow(1);

		boolean actual = hotels.hotelDetailsLoaded();
		boolean expected = true;
		assertEquals(actual, expected);
		System.out.println("The selected Hotel is displayed successfully ");
	}


	@Test(priority=4,enabled=true, dependsOnMethods={"HotelSelection"})
	public void HotelRoomDetailsConfirmation()
	{
		hotels.EditThenBookNowSelectedHotel(1,1);  //Option Index
		boolean actual = hotels.extraBookingDetails();
		boolean expected = true;
		assertEquals(actual, expected);
		System.out.println("The selected Hotel ROOM with it's specification is booked successfully ");
	}


	@Test(priority=5,enabled=true, dependsOnMethods={"HotelRoomDetailsConfirmation"})
	public void BookHotelRoom()
	{
		//Add an extra then confirm the final booking
		hotels.confirmBookingHotelRoom();
		boolean actual = reservationPayment.reservationComplete();
		boolean expected = true;
		assertEquals(actual, expected);
		System.out.println("Booked complete and Payment method is pending...");
	}


	@Test(priority=6,enabled=true, dependsOnMethods={"BookHotelRoom"})
	public void PaymentMethodWithCreditCard()
	{
		boolean actual = reservationPayment.payByCreditCard("JustTestFN", "TestLN", "INVALID123");
		boolean expected = true;
		assertEquals(actual, expected);
		System.out.println("Invalid error for invalid credit card payment");
	}


	@Test(priority=7,enabled=true, dependsOnMethods={"PaymentMethodWithCreditCard"})
	public void PaymentOnArrival()
	{
		boolean actual = reservationPayment.payOnArrival();
		boolean expected = true;
		assertEquals(actual, expected);
		System.out.println("Pay on arrival done and the order is Reserved properly !!");
	}
}
